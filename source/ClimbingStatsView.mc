using Toybox.WatchUi;
using Toybox.Sensor;
using Toybox.Position;
using Toybox.Time;
using Toybox.Time.Gregorian;


class ClimbingStatsView extends WatchUi.View {

	hidden var mIndex; 
	hidden var hr; 
	hidden var altitude; 
	hidden var heightAboveGround;
	hidden var altitudeOnGround;  
	hidden var totalAscent_ft;
	hidden var mFonts;
	hidden var graph; 
	hidden var reportedAltitudes; 
	

    function initialize(index) {
        WatchUi.View.initialize();
        
        Sensor.setEnabledSensors( [Sensor.SENSOR_HEARTRATE] );
    	Sensor.enableSensorEvents( method( :onSensor ) );
    	Position.enableLocationEvents(Position.LOCATION_CONTINUOUS, method(:onPosition));
    	
        
        mIndex = index;
        hr = "---";
        altitude = "---";
        heightAboveGround = 0; 
        totalAscent_ft = 0; 
        altitudeOnGround = 0; 
        reportedAltitudes = [null];
        
        graph = new LineGraph(30, 10, Graphics.COLOR_WHITE); 
        mFonts = [Graphics.FONT_SMALL, Graphics.FONT_MEDIUM, Graphics.FONT_LARGE];
    }
    
    function onSensor(sensorInfo) {
    	hr = sensorInfo.heartRate == null ? "---" : sensorInfo.heartRate; 
    	WatchUi.requestUpdate();    	
	}
	
	function onPosition(posInfo) {
		altitude = posInfo.altitude == null ? "---" : posInfo.altitude.format("%0.2f"); //(posInfo.altitude * 3.28084).format("%0.2f");  // default altitude is in meters
 		WatchUi.requestUpdate(); 
		
	}

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() { }

    // Update the view
    function onUpdate(dc) {
   		dc.setColor( Graphics.COLOR_TRANSPARENT, Graphics.COLOR_BLACK );
        dc.clear();
        
    	var width = dc.getWidth();
        var height = dc.getHeight();
        var marginTop = 8;
        var marginMid = height/2 - 5;
        var marginLeft = 8;
        var fWidth = 0;
        var text = "";
        var font = Graphics.FONT_TINY;
		var today = Gregorian.info(Time.now(), Time.FORMAT_LONG);
		var time = Lang.format(
		    "$1$:$2$",
		    [
		        today.hour.format("%02d"),
		        today.min.format("%02d")
		    ]
		);
        
        calculateChanges(); 
        
        if (!altitude.toString().equals("---")) {	        
        	
        	dc.setColor(Graphics.COLOR_GREEN, Graphics.COLOR_TRANSPARENT);
            graph.draw(dc, [(width / 9) - 2, 3], [width + 3, (height / 2) + 3]); //[(width / 9), 0], [(width/9) + 8, (height / 2) +4]);
            dc.setColor(Graphics.COLOR_GREEN, Graphics.COLOR_TRANSPARENT);
            text = "Altitude"; 
            dc.drawText(width/2, marginTop - 1, Graphics.FONT_TINY, text, Graphics.TEXT_JUSTIFY_CENTER);
        }
        
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        text = time + "\n"; 
        text += "Above Ground: " + heightAboveGround.format("%0.1f") + " m\n"; 
        text += "Total Climbed: " + totalAscent_ft.format("%0.1f") + " m\n";
        text += "HR: " + hr + " bpm\n";
        
        fWidth = dc.getTextWidthInPixels(text, font);
        dc.drawText(width/2, marginMid+3, Graphics.FONT_TINY, text, Graphics.TEXT_JUSTIFY_CENTER);
    }  
    
    
    function calculateChanges() {
    	if (!altitude.toString().equals("---")) {
    		var currentAltitude = altitude.toFloat();
  
        	var lastAltitude;
        	if (reportedAltitudes[0] == null) {
        		altitudeOnGround = currentAltitude;  // first altitude of activity
        		lastAltitude = currentAltitude; 
        		reportedAltitudes[0] = currentAltitude; 
        	} else {
        		if (altitudeOnGround.toFloat() > currentAltitude.toFloat()) { // should only apply to simulated data
        			currentAltitude = altitudeOnGround; 
        		}
        		lastAltitude = reportedAltitudes[reportedAltitudes.size() - 1]; 
        		if (!currentAltitude.format("%0.2f").equals(lastAltitude.format("%0.2f"))) {
        			reportedAltitudes.add(currentAltitude); 
        			graph.addItem(currentAltitude.toFloat()); 
        		}
        	}  
        	var gain_ft = (currentAltitude - lastAltitude) >= 0 ? (currentAltitude - lastAltitude) : 0; 
        	totalAscent_ft += gain_ft; 
        	heightAboveGround = currentAltitude - altitudeOnGround;
    	}
    } 

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() { }

}
