using Toybox.Application;
using Toybox.WatchUi;
using Toybox.Sensor;
using Toybox.Position; 

class ClimbingApp extends Application.AppBase {

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state) {
    	
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
   		
    }

    // Return the initial view of your application here
    function getInitialView() {
    //The initial view is located at index 0
        var index = 0;
        return [new StartView(index), new ClimbingDelegate(index)];
    }

}
