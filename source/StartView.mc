using Toybox.WatchUi;
using Toybox.System;


class StartView extends WatchUi.View {

	hidden var mIndex; 
	hidden var mIndicator;
	hidden var mHeadingFont;
	hidden var mDataFont;
	hidden var mUnitFont; 

    function initialize(index) {
        WatchUi.View.initialize();

        mIndex = index;

        mHeadingFont = Graphics.FONT_SMALL;
        mDataFont = Graphics.FONT_LARGE;
        mUnitFont = Graphics.FONT_TINY;
      
    }


    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }
 

    // Update the view
    function onUpdate(dc) {
   		dc.setColor( Graphics.COLOR_TRANSPARENT, Graphics.COLOR_BLACK );
        dc.clear();
    	var width = dc.getWidth();
        var height = dc.getHeight();
        var marginTop = 8;
        var marginMid = height/2 - 5;
        var marginLeft = 8;
        var fWidth = 0;
        var text;
        var font = Graphics.FONT_TINY;
        
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
        text = "\nPage down\nto start!\n\nTo save as an activity\npress the Select button!";
        fWidth = dc.getTextWidthInPixels(text, font);
        dc.drawText(width/2, marginTop+ 3, font, text, Graphics.TEXT_JUSTIFY_CENTER);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

}
