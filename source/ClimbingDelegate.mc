using Toybox.WatchUi;
using Toybox.ActivityRecording;


class ClimbingDelegate extends WatchUi.BehaviorDelegate {

	var mIndex;
	var session = null;  

    function initialize(index) {
        BehaviorDelegate.initialize();
        mIndex = index;
    }
    
    /*function onSelect() {
    	WatchUi.switchToView(getView(mIndex), getDelegate(mIndex), WatchUi.SLIDE_LEFT);
    }*/
    /* From Garmin SDK Documentation Example */ 
    function onSelect() {
	   if (Toybox has :ActivityRecording) {                          // check device for activity recording
	       if ((session == null) || (session.isRecording() == false)) {
	           session = ActivityRecording.createSession({          // set up recording session
	                 :name=>"Climbing",                              // set session name
	                 :sport=> ActivityRecording.SPORT_GENERIC, //ActivityRecording.SPORT_ROCK_CLIMBING, // set sport type
	                 :subSport=>ActivityRecording.SUB_SPORT_GENERIC // set sub sport type
	           });
	           session.start();                                     // call start session
	           System.println("Started Session"); 
	       }
	       else if ((session != null) && session.isRecording()) {
	           session.stop();                                      // stop the session
	           session.save();                                      // save the session
	           System.println("Stopped and saved past session"); 
	           
	           session = null;                                      // set session control variable to null
	       }
	   }
	   return true;                                                 // return true for onSelect function
	}
	/* End Example */ 
    
    function onNextPage() {
    	mIndex = (mIndex + 1) % 2;
    	WatchUi.switchToView(getView(mIndex), getDelegate(mIndex), WatchUi.SLIDE_LEFT);
    }
    
     function onPreviousPage() {
    	mIndex = mIndex - 1;
        if (mIndex < 0) {
            mIndex = 2;
        }
        mIndex = mIndex % 2;
        WatchUi.switchToView(getView(mIndex), getDelegate(mIndex), WatchUi.SLIDE_LEFT);
    }

	function getView(mIndex) {
        var view;

        if (0 == mIndex) {
            view = new StartView(mIndex);
        } else {
            view = new ClimbingStatsView(mIndex);
        } 
        return view;
    }

    function getDelegate(mIndex) {
        return new ClimbingDelegate(mIndex);
    }
}
